// For outputting data or text into the browser console
console.log('Hello World!')

//[SYNTAX AND STATEMENTS]
//Syntax is the code that makes up a statement
/*console
log()*/

//Statements are made up of syntax that will be run by the browser
//console.log()

//[COMMENTS]
// This is a single-line comment

/*This is a multiple-line comment for longer descriptions 
and multi-line paragraphs
*/

//[Casing]
/*camelCasing
FascalCasing
snake_casing*/

//[VARIABLES]
//let variables are variables that can be re-assigned
let firstName = 'Amiel';
console.log(firstName);

let lastName = 'Oliva';
console.log(lastName);

// Re-assigning a value to a let variable shows no errors
firstName = 'Elon';
console.log(firstName)

// const variables are variables that CANNOT be re-aasigned
const colorOfTheSun ='yellow';
console.log('The color of the sun is ' + colorOfTheSun)

// Error happens when you try to re-assign value of a const variable
/*colorOfTheSun = 'red';
console.log(colorOfTheSun)*/

// When declaring a variable, you use a keyword like 'let'
let variableName = 'Value';

// When re-assigning a value to a variable, you ust need the variable name
variableName = 'New Value';


// [DATA TYPES]
// 1.String - Denote by single or double quotation marks
let personName = 'Amiel Oliva';

// 2. Number - No quotation marks and numerical value
let personAge = 18;

// 3. Boolean - Only 'true' or 'false'
let hasGirlfriend = false;

// 4.Array - Denoted by brackets and can contain multiple values inside
let hobbies = ['Playing', 'Watching', 'Coding'];

// 5. Object - Denoted by curly braces and has value name/label for each value.
let person = {
	personName: 'Amiel Oliva',
	personAge: 18,
	hasGirlfriend: false,
	hobbies: ['Playing', 'Watching', 'Coding'],
}


// 6.Null - A placeholder for future variable re-assignments
let wallet = null;

//console.log each of the variables

console.log(personName);
console.log(personAge);
console.log(hasGirlfriend);
console.log(hobbies);
console.log(person);
console.log(wallet);

// to display single value of an object
console.log(person.personAge);

//to display single value of an array
console.log(hobbies[1]);